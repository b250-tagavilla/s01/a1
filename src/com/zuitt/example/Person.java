package com.zuitt.example;

import java.nio.DoubleBuffer;
import java.util.Scanner;
public class Person {
    public static void main(String[] args) {
        String firstName;
        String lastName;
        double firstSubject;
        double secondSubject;
        double thirdSubject;
        double fourthSubject;
        double average;
        Scanner scanner = new Scanner(System.in);

        System.out.println("First name:");
        firstName = scanner.nextLine();

        System.out.println("Last name:");
        lastName = scanner.nextLine();

        System.out.println("First Subject Grade");
        firstSubject = scanner.nextDouble();

        System.out.println("Second Subject Grade");
        secondSubject = scanner.nextDouble();

        System.out.println("Third Subject Grade");
        thirdSubject = scanner.nextDouble();

        System.out.println("Fourth Subject Grade");
        fourthSubject = scanner.nextDouble();

        average = (firstSubject + secondSubject + thirdSubject + fourthSubject ) / 4;
        System.out.println("Good day, " + firstName + " " + lastName + ".");
//        System.out.println("Your grade average is: " + Integer.parseInt(Double.toString(average).substring(0, 2)));
        System.out.println("Your grade average is: " + average);
    }
}
